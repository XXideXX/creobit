using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreSistem : MonoBehaviour
{
    string scoreKey = "Score";
    public int currentScore { get; set; }
    private void Awake()
    {
        currentScore = PlayerPrefs.GetInt(scoreKey);

    }
    public void SetScore(int score)
    {
        PlayerPrefs.SetInt(scoreKey, score);
        currentScore = PlayerPrefs.GetInt(scoreKey);
    }
    public void ResetScore()
    {

        SetScore(0);

    }
}
