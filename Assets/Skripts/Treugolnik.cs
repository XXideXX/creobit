using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treugolnik : Interacting
{
    private bool attached = false;
    private GameObject lastgo;


    void Update()
    {
        if (select == true)
        {
            transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {

        if (select == false && collision.tag == "square" && attached == false && lastgo != collision.gameObject)
        {

            Pitaensyasoedenit(collision.gameObject);

        }
    }

    private void Pitaensyasoedenit(GameObject obj)
    {

        if (gameMananger.energy > 0)
        {
            gameMananger.UmenshitColichestvoEnergii();
            obj.GetComponent<IzmenenieRazmera>().razmer -= 1;
            Destroy(gameObject);
        }

    }
}

