using UnityEngine;
using System.Collections;

public class DetectItems : MonoBehaviour
{
    Ray ray;
    RaycastHit2D hit;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            onHover(ray, hit);
        }
    }

    public void onHover(Ray ray, RaycastHit2D hit)
    {
        hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
        if (hit.collider == null)
        {
            //Debug.Log("nothing hit");
        }
        else
        {
            hit.collider.GetComponent<Interacting>().OnMoseClick();
        }
    }
}
