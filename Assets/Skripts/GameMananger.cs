
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public  class GameMananger : MonoBehaviour
{
    public  int turns;
    [SerializeField] private Text turnsLable;
    [Space] 
    [SerializeField] private Text energyLable;
    [SerializeField] private GameObject energulabel;
    public int energy;
   
    private void Start()
    {
        turnsLable.text = "Moves " + turns.ToString();
        if (energyLable != null)
        {
            energulabel.SetActive(true);
            energyLable.text = "Energy " + energy.ToString();

        }
    }
  

    public  void UmenshitColichestvoHodow()
    {
        turns -= 1;
        turnsLable.text = "Moves " + turns.ToString();
        if(turns == 0)
        EndGame();
    }
    public void UmenshitColichestvoEnergii()
    {
        energy -= 1;
        energyLable.text = "Energy " + energy.ToString(); 
    }


    private void EndGame()
    {
        SceneManager.LoadScene(0);
    }
}

