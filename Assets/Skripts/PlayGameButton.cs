
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayGameButton : MonoBehaviour
{
    [SerializeField] private int lvl;
    public void PlayGame ()
    {
        SceneManager.LoadScene(lvl);
    }
}
