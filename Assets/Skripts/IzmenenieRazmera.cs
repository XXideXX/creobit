using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class IzmenenieRazmera : MonoBehaviour
{

    public int razmer;
    [SerializeField] private bool editing;
    [SerializeField] private Vector3 defoltLocalSkale;

  
    void Update()
    {
        
        if(editing )
        { 
           transform.localScale = defoltLocalSkale*razmer;
        }
    }
}
