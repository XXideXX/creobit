using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LvlMenu : MonoBehaviour
{
    [SerializeField] private ScoreSistem skoreSistem;
    [SerializeField] private Text text;
    private void Start()
    {
        UpdateCvadratikiData();
    }
    public void UpdateCvadratikiData()
    {

        text.text = "Vsego cvadraticov slozil " + skoreSistem.currentScore;

    }    
}
